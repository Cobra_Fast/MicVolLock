# Windows Microphone Volume Locker

## Motivation

Nowadays there seems to be a trend towards assuming your run of the mill communications program knows better than the user at which level the microphone volume should be set, leading to annoying situations when switching to a different communications program that either doesn't change the microphone level or prefers a different one.

## Solution

Employing Audio Switcher's CoreAudio package, this simple tray program will capture attempts to change the microphone volume and immediately set it back to the preferred level.

Additionally a background thread can be started that resets the microphone level periodically.

## Configuration

Currently hardcoded as constants in `Program.cs`.

## Additional credit

* Fugue iconset by Yusuke Kamiyamane <https://p.yusukekamiyamane.com/>.
* WPF NotifyIcon by Hardcodet <https://www.nuget.org/packages/Hardcodet.NotifyIcon.Wpf/>
* Audio Switcher CoreAudio by xenolightning <https://www.nuget.org/packages/AudioSwitcher.AudioApi.CoreAudio/>
