﻿using System;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Windows;
using System.Drawing;
using System.Threading;
using AudioSwitcher.AudioApi;
using AudioSwitcher.AudioApi.CoreAudio;
using Hardcodet.Wpf.TaskbarNotification;
using Microsoft.Win32;

namespace TrayRunner
{
	class Program
	{
		public const double PreferredLevel = 90.0;  // level to set to. (0..100)
		public const bool HandleChanges = true;		// capture volume change events and reset if necessary.
		public const bool RunSetter = true;		// run periodic volume reset.
		public const int SleepInterval = 15000;      // milliseconds between volume resets.

		private static MicLocker micLocker = null;

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		public static void Main(string[] Args)
		{
			var app = new Application();
			app.Startup += App_Startup;
			app.SessionEnding += App_SessionEnding;
			app.Run();
		}

		private static void App_Startup(object sender, StartupEventArgs e)
		{
			TaskbarIcon tbi = new TaskbarIcon();
			tbi.Icon = Icon.FromHandle(Resource.microphone_lock.GetHicon());
			tbi.ToolTipText = @"Microphone Volume Locker";

			micLocker = new MicLocker();
		}

		private static void App_SessionEnding(object sender, SessionEndingCancelEventArgs e)
		{
			micLocker.Shutdown();
		}
	}

	internal class MicLocker : IObserver<DeviceVolumeChangedArgs>
	{
		private readonly CoreAudioController audioController;
		private readonly CoreAudioDevice defaultComDevice;
		private readonly IDisposable volumeChangedSubscription;
		private readonly Thread resetThread;

		[SuppressMessage("ReSharper", "ConditionIsAlwaysTrueOrFalse")]
		public MicLocker()
		{
			this.audioController = new CoreAudioController();
			this.defaultComDevice = this.audioController.DefaultCaptureCommunicationsDevice;
			Debug.WriteLine($"Working with audio device '{this.defaultComDevice.FullName}'.");

			if (Program.HandleChanges)
				this.volumeChangedSubscription = this.defaultComDevice.VolumeChanged.Subscribe(this);

			if (Program.RunSetter)
			{
				this.resetThread = new Thread(periodicResetter);
				this.resetThread.Start();
			}
		}

		private void periodicResetter()
		{
			while (true)
			{
				Debug.WriteLine($"Setting volume to {Program.PreferredLevel}.");
				//this.defaultComDevice.Volume = Program.PreferredLevel;
				this.defaultComDevice.SetVolumeAsync(Program.PreferredLevel).Wait();
				Thread.Sleep(Program.SleepInterval);
			}
		}

		public void Shutdown()
		{
			this.volumeChangedSubscription.Dispose();
			this.resetThread?.Abort();
		}

		public void OnNext(DeviceVolumeChangedArgs value)
		{
			if (value.ChangedType != DeviceChangedType.VolumeChanged || value.Device.Id != this.defaultComDevice.Id)
				return;

			Debug.WriteLine($"Caught volume change to {value.Volume}.");

			if (Math.Abs(value.Volume - Program.PreferredLevel) > 0.1)
			{
				Debug.WriteLine($"Resetting volume to {Program.PreferredLevel}!");
				//this.defaultComDevice.Volume = Program.PreferredLevel;
				this.defaultComDevice.SetVolumeAsync(Program.PreferredLevel).Wait();
			}
		}

		public void OnError(Exception error)
		{ }

		public void OnCompleted()
		{ }
	}
}
